import bpy

from bpy.props import BoolProperty


class CollectionExportSettings(bpy.types.PropertyGroup):
    ToExport: BoolProperty(
        name='Boolean to export',
        default=False,
    )

