import bpy


# -----------------------------------------------------------------------------
# Export Panel
# -----------------------------------------------------------------------------
class MESH_PT_ExportUE4(bpy.types.Panel):
    bl_idname = 'MESH_PT_ExportUE4'
    bl_label = 'Exports Batch'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'GA Tools'

    def draw(self, context):
        layout = self.layout

        layout.prop(context.scene.ue_path, 'export_folder')
        layout.operator("object.exports_batch", text='Exports Scenes', icon='FILE_TICK')
        layout.operator("object.data_buffer", text='Copy Data Buffer', icon='COPYDOWN')
        # layout.operator("object.manage_assets", text='Assets Elements', icon='MATCUBE')
        layout.operator("object.spline_csv", text='Export Curve Data', icon='CURVE_DATA')
        layout.label(text='Buffer Location')
        
        split = layout.split(align=True)
        loc_copy = split.operator('object.relat_loc', text='Copy Loc.', icon='COPYDOWN')
        loc_copy.copy = True
        loc_paste = split.operator('object.relat_loc', text='Paste Loc.', icon='PASTEDOWN')
        loc_paste.copy = False
